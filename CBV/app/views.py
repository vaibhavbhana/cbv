from django.shortcuts import render
from .models import Contact 
from .forms import ContactForm 
from django.views.generic.edit import CreateView  
from django.views.generic.list import ListView  
from django.views.generic.edit import UpdateView  
from django.views.generic.detail import DetailView 
from django.views.generic.edit import DeleteView  
  
class ContactCreate(CreateView):  
    model = Contact 
    success_url = "/"
  
    fields = '__all__'  

class ContactRetrieve(ListView):  

    model = Contact

 
  
class ContactDetail(DetailView):  
    model = Contact


class ContactUpdate(UpdateView): 

    form_class=ContactForm
    model = Contact  


class ContactDelete(DeleteView):  
    model = Contact
    success_url = '/'  
