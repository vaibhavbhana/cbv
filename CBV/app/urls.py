from django.urls import path
from . import views

urlpatterns = [
    path('',views.ContactCreate.as_view(), name = 'ContactCreate'),
    path('<int:pk>', views.ContactDetail.as_view(), name = 'EmployeeDetail'),  
    # path('retrieve/', views.ContactRetrieve.as_view(), name = 'ContactRetrieve')
    path('<int:pk>/update/', views.ContactUpdate.as_view(), name = 'EmployeeUpdate'),  
    path('<int:pk>/delete/', views.ContactDelete.as_view(), name = 'ContactDelete')

]
